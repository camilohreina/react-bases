// Variables y constantes

const nombre = "Camilo";
const apellido = "Hernandez";

let valorDado = 5;
valorDado = 3;

// Var no se debe de usar...

console.log(nombre, apellido, valorDado);

if (true) {
  const nombre = "Peter";
  let valorDado = 6;

  console.log(nombre);
  console.log(valorDado);
}

console.log(valorDado);
