const getImage = async () => {
  try {
    const apiKey = "6eUMfF9lQsuUStWJK4AIMReyIbeqVG71";
    const res = await fetch(
      `http://api.giphy.com/v1/gifs/random?api_key=${apiKey}`
    );
    const { data } = await res.json();
    const { url } = data.images.original;
    const img = document.createElement("img");
    img.src = url;
    document.body.append(img);
  } catch (error) {
    // Manejo del error
  }
};

getImage();
