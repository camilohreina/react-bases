const persona = {
  nombre: "Camilo",
  apellido: "Hernandez",
  edad: 45,
  direccion: {
    ciudad: "Cali",
    zip: 52541,
    lan: 14.541,
    lng: 34.55124,
  },
};

// console.table(persona);

// Con los ... se copian todas las index de un objeto pero sin sus valores
const persona2 = { ...persona };
persona2.nombre = "Peter";
console.log(persona);
console.log(persona2);
