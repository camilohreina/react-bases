const personajes = ["Goku", "Vegetta", "Trunks"];

const [, , p2] = personajes;

console.log(p2);

const retornaArreglo = () => {
  return ["ABC123", 123];
};
const [letras, numeros] = retornaArreglo();
console.log(letras, numeros);

const State = (valor) => {
  return [
    valor,
    () => {
      console.log("Hola mundo");
    },
  ];
};

const [nombre, setNombre] = State("Goku");
console.log(nombre);
setNombre();
