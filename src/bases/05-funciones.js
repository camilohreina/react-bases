// Funciones en JS

// const saludar = function (nombre) {
//   return `Hola, ${nombre}`;
// };

const saludar2 = (nombre) => {
  return `Hola, ${nombre}`;
};

const saludar3 = (nombre) => `Hola, ${nombre}`;
const saludar4 = () => `Hola, Mundo`;

console.log(saludar2("Goku"));
console.log(saludar3("Vegetta"));
console.log(saludar4("Vegetta"));

// Return un objeto en una sola linea
const getUser = () => ({
  uid: "ABC123",
  username: "elpapi",
});

const user = getUser();

console.log(user);

const getUsuarioActivo = (nombre) => ({
  uid: "23231",
  username: nombre,
});

const usuarioActivo = getUsuarioActivo("Mila");
console.log(usuarioActivo);
